#!/bin/bash

set -e
set -o xtrace

export DEBIAN_FRONTEND=noninteractive

apt-get update

apt-get install -y \
	apt-transport-https \
	ca-certificates \
	wget \
	libdrm-dev \
	x11proto-dev \
	meson \
	git

echo 'deb-src https://deb.debian.org/debian buster main' > /etc/apt/sources.list.d/deb-src.list
echo 'deb-src https://deb.debian.org/debian buster-updates main' >> /etc/apt/sources.list.d/deb-src.list
apt-get update
apt-get build-dep -y xvfb
git clone --single-branch --branch sent/xvfb-render-node https://gitlab.freedesktop.org/evelikov/xserver
pushd xserver
meson build/ \
    -D ipv6=true \
    -D dmx=false \
    -D xvfb=true \
    -D xnest=false \
    -D xcsecurity=true \
    -D xorg=false \
    -D xephyr=false \
    -D xwayland=false \
    -D glamor=true \
    -D udev=true \
    -D systemd_logind=false \
    -D suid_wrapper=false \
    -D xkb_bin_dir=/usr/bin \
    -D xkb_dir=/usr/share/X11/xkb \
    -D xkb_output_dir=/var/lib/xkb

ninja -C build/
ninja -C build/ install
popd
rm -rf xserver
rm /etc/apt/sources.list.d/deb-src.list
apt-get update

# Download and run upstream Mesa debian-install.sh to install all dependencies
# for building Mesa
TMPDIR=$(mktemp -d)
wget -O "$TMPDIR/debian-install.sh" 'https://gitlab.freedesktop.org/mesa/mesa/raw/master/.gitlab-ci/debian-install.sh?inline=false'
bash "$TMPDIR/debian-install.sh"
rm -rf "$TMPDIR"

# Sanity check we're using the local mesa $glxinfo | grep $MESA_SHA
apt-get install -y mesa-utils

# Renderdoc and apitrace
export APITRACE_VERSION=0f541f460bba86a69c8c714076db5a7607cc8d16
export RENDERDOC_VERSION=v1.4
apt-get install -y git xvfb cmake autoconf gcc g++ make python3-pil python3-pilkit
apt-get install -y libx11-dev libx11-xcb-dev mesa-common-dev libgl1-mesa-dev libxcb-keysyms1-dev \
                 cmake python3-dev bison autoconf automake libpcre3-dev

git clone https://github.com/apitrace/apitrace.git
pushd apitrace
git checkout $APITRACE_VERSION
cmake -B_build -H. -DCMAKE_BUILD_TYPE=Release
make -C _build install
popd
rm -rf apitrace

git clone --depth=1 --branch $RENDERDOC_VERSION https://github.com/baldurk/renderdoc.git
pushd renderdoc
cmake -B_build -H. -DENABLE_QRENDERDOC=false -DCMAKE_BUILD_TYPE=Release
make -C _build install
mkdir -p /usr/local/lib/$(py3versions -d)/site-packages
cp _build/lib/renderdoc.so /usr/local/lib/$(py3versions -d)/site-packages
popd
rm -rf renderdoc
