#!/usr/bin/python3

# Copyright (c) 2019 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import argparse
import os
import sys
import subprocess
from pathlib import Path
from renderdoc_dump_images import renderdoc_dump_images
from traceutil import iter_trace_paths, trace_has_images

def log(severity, msg):
    print("[dump_images] %s: %s" % (severity, msg))

def get_calls_num(trace, device_name):
    tracename = str(Path(trace).name)
    refdir = str(Path(trace).parent / "references" / device_name)
    calls = []
    for root, dirs, files in os.walk(refdir):
        for file in files:
            if file.startswith(tracename) and file.endswith(".png"):
                callnum = file[(len(tracename) + 1):-4]
                calls.append(callnum)
    return calls

def dump_with_apitrace(trace, calls, device_name):
    outputdir = str(Path(trace).parent / "test" / device_name)
    os.makedirs(outputdir, exist_ok=True)
    outputprefix = str(Path(outputdir) / Path(trace).name) + "-"
    cmd = ["apitrace", "dump-images", "--calls=" + ','.join(calls),
           "-o", outputprefix, trace]
    ret = subprocess.call(cmd)
    if ret:
        raise RuntimeError("Apitrace dump-images failed!")

def dump_with_renderdoc(trace, calls, device_name):
    outputdir = str(Path(trace).parent / "test" / device_name)
    renderdoc_dump_images(trace, [int(c) for c in calls], outputdir);

def dump_from_trace(trace, device_name):
    log("Info", "Dumping trace %s" % trace)
    calls = get_calls_num(trace, device_name)
    if trace.endswith('.trace'):
        dump_with_apitrace(trace, calls, device_name)
    elif trace.endswith('.rdc'):
        dump_with_renderdoc(trace, calls, device_name)
    else:
        raise RuntimeError("Unknown tracefile extension")

def find_traces_with_reference_images(directory, device_name):
    traces = []
    for trace_path in iter_trace_paths(directory):
        if trace_has_images(trace_path, str(Path("references") / device_name)):
            log("Info", "Adding %s in list of traces to dump" % str(trace_path))
            traces.append(str(trace_path))
        else:
            log("Warning", "%s has no reference images, skipping" % str(trace_path))
    return traces

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('tracepath', help="single trace or dir to walk recursively")
    parser.add_argument('--device-name', required=True,
                        help="the name of the graphics device used to produce images")

    args = parser.parse_args()

    traces = []
    if os.path.isdir(args.tracepath):
        traces.extend(find_traces_with_reference_images(args.tracepath, args.device_name))
    elif os.path.isfile(args.tracepath):
        traces.append(args.tracepath)

    for trace in traces:
        dump_from_trace(trace, args.device_name)

if __name__ == "__main__":
    main()
