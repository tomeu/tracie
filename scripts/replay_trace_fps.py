#!/usr/bin/python3

# Copyright (c) 2019 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import os
import re
import sys
import subprocess
from pathlib import Path
from traceutil import iter_trace_paths

def log(severity, msg):
    print("[replay_traces] %s: %s" % (severity, msg))

def parse_output(output):
    match = re.search('(\d+.\d+) fps', output)
    if not match:
        raise RuntimeError("Result line can't be located")
    return match[1]

def replay_from_trace(trace):
    NB_REPLAY = 3
    if trace.endswith('.trace'):
        log("Info", "Replaying trace %s" % trace)
        max_fps = 0.0
        cmd = ["apitrace", "replay", "-b", trace]
        for x in range(0, NB_REPLAY):
            ret = subprocess.check_output(cmd).decode('UTF-8')
            if ret:
                fps = float(parse_output(ret))
                max_fps = max(fps, max_fps)
            else:
                raise RuntimeError("Apitrace replay failed!")
        print(trace + ": Best of " + str(NB_REPLAY) + " replay runs: " + str(max_fps) + " FPS")
    else:
        log("Info", "Extension not supported yet %s" % trace)

def find_traces(directory):
    traces = []
    for trace_path in iter_trace_paths(directory):
        traces.append(str(trace_path))
    return traces

def main():
    if len(sys.argv) != 2:
        raise RuntimeError("usage: replay_trace_fps <tracefile or dir>")

    arg = sys.argv[1]

    traces = []
    if os.path.isdir(arg):
        traces.extend(find_traces(arg))
    elif os.path.isfile(arg):
        traces.append(arg)

    for trace in traces:
        replay_from_trace(trace)

if __name__ == "__main__":
    main()
