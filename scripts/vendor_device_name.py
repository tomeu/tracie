#!/usr/bin/env python3

# Copyright (c) 2019 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import sys
import subprocess
import re

def is_vendor_string(s):
    return "Vendor:" in s

def is_device_string(s):
    return "Device:" in s

def id_from_string(s):
    match = re.search("\((0x[0-9A-Fa-f]+)\)$", s)
    if match:
        return match[1]
    return None

def vendor_from_string(s):
    if "Intel" in s:
        return "intel"
    elif "VMware" in s:
        return "vmware"
    else:
        return id_from_string(s)

def device_from_string(s):
    if "softpipe" in s:
        return "softpipe"
    elif "llvmpipe" in s:
        return "llvmpipe"
    else:
        return id_from_string(s)

vendor = None
device = None

res = subprocess.run(["glxinfo"], capture_output=True)

for l in res.stdout.decode("utf-8").splitlines():
    if is_vendor_string(l):
        vendor = vendor_from_string(l)
    elif is_device_string(l):
        device = device_from_string(l)

    if vendor is not None and device is not None:
        break

if vendor is None or device is None:
    sys.exit(1)

print("%s-%s" % (vendor, device))
